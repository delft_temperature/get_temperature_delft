import requests

url='https://weerindelft.nl/clientraw.txt'

try:
    response = requests.post(url)
except Exception as e:
     print(f"Failed to get requets from {url} due to error: {e}")

try:
    temperature=round(float(list(str(response.content).split(" "))[4]))
except Exception as e:
    print("Failed to process the value for the temperature due to error: {e}")

print(f"{temperature} degree Celsius")
