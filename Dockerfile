FROM python:3.7.3-stretch
COPY requirements.txt .
RUN pip3 install -U -r requirements.txt
COPY HoeWarmIsHetInDelft.py .
